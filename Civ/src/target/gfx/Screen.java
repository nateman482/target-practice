package target.gfx;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Screen {

	private Graphics g;
	private BufferedImage[] sprites;
	
	public int width, height;
//	public int xoffs = 0, yoffs = 0;
	
	public Screen(int width, int height, Graphics g) {
		this.width = width;
		this.height = height;
		this.g = g;
		
		g.setFont(new Font(null, 0, 15));
		sprites = new Spritesheet().getSpritesheet("/sprites.png");
	}
	
	public void drawString(String string, int x, int y, int size) {
		Font font = new Font(null, 0, size);
		g.setFont(font);
		
		g.drawString(string, x, y);
	}
	
	
	
	public void drawRect(boolean fill, int x, int y, int width, int height) {
		if (fill) {
			g.fillRect(x, y, width, height);
		} else {
			g.drawRect(x, y, width, height);
		}
	}
	
	public void setColor(Color color) {
		g.setColor(color);
	}
	
//	public void draw(int sprite, int xTile, int yTile) {
//		g.drawImage(sprites[sprite], 10 + xTile * 16 + xoffs, 10 + yTile * 16 + yoffs, 16, 16, null);
//	}
	
//	public void setOffs(int xoffs, int yoffs) {
//		this.xoffs = xoffs;
//		this.yoffs = yoffs;
//	}
}
