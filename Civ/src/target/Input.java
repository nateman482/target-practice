package target;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.HashMap;

public class Input implements KeyListener, MouseListener, MouseMotionListener {
	
	public boolean[] downkeys = new boolean[65536];
	public boolean[] togglekeys = new boolean[65536];
	public boolean[] busykeys = new boolean[65536];
	
	public HashMap<String, Integer> controls = new HashMap<String, Integer>();
	
	public int lastPressedKey = 0;
	
	public int cursorX = 0, cursorY = 0;
	public int clickX = 0, clickY = 0, lastClickX = 0, lastClickY = 0;
	public int xoffs = 0, yoffs = 0;
	public boolean clicked = false, blockClick = false;
	public boolean dragging;
	
	private int width, height;
	
	public Input(BoilerPlate game, int width, int height, int scale) {
		this.width = width * scale;
		this.height = height * scale;
		game.addKeyListener(this);
		game.addMouseListener(this);
		game.addMouseMotionListener(this);
		
		controls.put("up", KeyEvent.VK_UP);
		controls.put("left", KeyEvent.VK_LEFT);
		controls.put("down", KeyEvent.VK_DOWN);
		controls.put("right", KeyEvent.VK_RIGHT);
		controls.put("space", KeyEvent.VK_SPACE);
		controls.put("escape", KeyEvent.VK_ESCAPE);
		controls.put("enter", KeyEvent.VK_ENTER);
	}
	
	public void tick() {
		if (clicked) clicked = false;
		
		for (int i = 0; i < 65536; i++) {
			if (togglekeys[i]) {
				togglekeys[i] = false;
				busykeys[i] = true;
			} else if (downkeys[i] && !togglekeys[i] && !busykeys[i]) {
				togglekeys[i] = true;
			}
			
			if (busykeys[i]) {
				if (!downkeys[i]) {
					busykeys[i] = false;
				}
			}
		}
	}
	
	@Override
	public void keyPressed(KeyEvent ke) {
		lastPressedKey = ke.getKeyCode();
		downkeys[ke.getKeyCode()] = true;
	}

	@Override
	public void keyReleased(KeyEvent ke) {
		downkeys[ke.getKeyCode()] = false;
	}

	@Override
	public void keyTyped(KeyEvent ke) {	}

	@Override
	public void mouseClicked(MouseEvent me) {
		int x = me.getX();
		int y = me.getY();
		if (x >= 30 && x < width - 18 && y >= 30 && y < height) {
			clickX = x;
			clickY = y;
			clicked = true;
		}
	}
	
	@Override
	public void mouseMoved(MouseEvent me) {
		cursorX = me.getX();
		cursorY = me.getY();
	}
	
	@Override
	public void mouseDragged(MouseEvent me) {
		cursorX = me.getX();
		cursorY = me.getY();
		
		if (cursorX >= 30 && cursorX < width - 18 && cursorY >= 30 && cursorY < height - 18) {
			if (!dragging) {
				dragging = true;
			} else {
				xoffs += (cursorX - lastClickX) / 2;
				yoffs += (cursorY - lastClickY) / 2;
			}
		}
		
		lastClickX = cursorX;
		lastClickY = cursorY;
	}
	
	@Override
	public void mouseReleased(MouseEvent me) {
		dragging = false;
		clicked = false;
		blockClick = false;
	}
	
	public boolean hasClicked() {
		return clicked;
	}

	@Override
	public void mouseEntered(MouseEvent me) {}

	@Override
	public void mouseExited(MouseEvent me) {}

	@Override
	public void mousePressed(MouseEvent me) {}
}