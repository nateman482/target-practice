package target;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.Random;

import javax.swing.JFrame;

import target.game.Game;
import target.game.TargetPracticeGame;
import target.game.menu.Menu;
import target.gfx.Screen;

public class BoilerPlate extends Canvas implements Runnable {

	public static Random r = new Random();
	public static int WIDTH = 244;
	public static int HEIGHT = 180; // 14 * 10 tiles + 10 pixels on every side
	public static int SCALE = 3;
	public static String TITLE = "Target Practice";
	
	private static final long serialVersionUID = -2661748013610271180L;
	private static JFrame frame;
	
	private boolean running = false;
	private boolean paused = false;
	private boolean inGame = false;
	
	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
	private Input input;
	private Screen screen;
	private Game menu, game;
	
	public static void main(String[] args) {
		BoilerPlate bp = new BoilerPlate();
		bp.setMinimumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		bp.setMaximumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		bp.setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));

		frame = new JFrame(TITLE);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(bp);
		frame.pack();
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setIgnoreRepaint(true);
		bp.start();
	}

	public void start() {
		requestFocus();
		running = true;
		new Thread(this).start();
	}

	public void stop() {
		running = false;
	}
	
	public void init() {
		input = new Input(this, WIDTH, HEIGHT, SCALE);
		screen = new Screen(WIDTH, HEIGHT, image.getGraphics());
		menu = new Menu();
	}

	@Override
	public void run() {
		createBufferStrategy(2);
		BufferStrategy bs = getBufferStrategy();

		init();

		while (running) {
			final double GAME_HERTZ = 60.0;
			final double TIME_BETWEEN_UPDATES = 1000000000 / GAME_HERTZ;
			final int MAX_UPDATES_BEFORE_RENDER = 1;
			double lastUpdateTime = System.nanoTime();

			while (running) {
				double now = System.nanoTime();
				int updateCount = 0;

				if (!paused) {
					while (now - lastUpdateTime > TIME_BETWEEN_UPDATES && updateCount < MAX_UPDATES_BEFORE_RENDER) {
						tick();
						lastUpdateTime += TIME_BETWEEN_UPDATES;
						updateCount++;
					}

					render(bs);
					
					Thread.yield();
					now = System.nanoTime();
				}
			}
		}

		System.exit(0);
	}

	private void tick() {
		if (inGame) {
			game.tick(input);
			if (Game.stopping) {
				Game.stopping = false;
				inGame = false;
			}
		} else {
			menu.tick(input);
			if (Game.exiting) stop();
			if (Game.starting) {
				Game.starting = false;
				inGame = true;
				
				game = new TargetPracticeGame();
			}
		}
		input.tick();
//		screen.setOffs(input.xoffs, input.yoffs);
	}
	
	private void render(BufferStrategy bs) {
		do {
			do {
				Graphics g = image.getGraphics();
				g.clearRect(0, 0, WIDTH, HEIGHT);
				
				if (inGame) {
					game.render(screen);
				} else {
					menu.render(screen);
				}
				
				g = bs.getDrawGraphics();
				g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
				g.dispose();

			} while (bs.contentsRestored());

			bs.show();

		} while (bs.contentsLost());
	}
	
}
