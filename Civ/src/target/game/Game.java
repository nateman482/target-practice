package target.game;

import target.Input;
import target.gfx.Screen;

public class Game {

	public static boolean starting = false, stopping = false, exiting = false, running = false;
	
	public Game() {
	}

	public void tick(Input input) {
	}
	
	public void render(Screen screen) {
	}
	
	public void stop() {
		running = false;
		stopping = true;
	}
	
	public void exit() {
		exiting = true;
	}
	
	public void startGame() {
		starting = true;
	}
}
