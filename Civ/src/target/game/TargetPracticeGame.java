package target.game;

import target.Input;
import target.gfx.Screen;

public class TargetPracticeGame extends Game {

	// Variables go here
	
	public TargetPracticeGame() {
		super();
	}
	
	@Override
	public void tick(Input input) {
		if (input.togglekeys[input.controls.get("escape")]) stop();
	}
	
	@Override
	public void render(Screen screen) {
		
	}
}
