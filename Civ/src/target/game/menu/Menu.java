package target.game.menu;

import target.Input;
import target.game.Game;
import target.gfx.Screen;

public class Menu extends Game {

	public Menu() {
		super();
	}
	
	private boolean scrollText = true;
	private int x1 = 240, y1 = 40, x2 = -160, y2 = 60; // for scrolling intro
	private int menuOptions = 1;
	private int index = menuOptions;
	
	public int game;
	
	@Override
	public void tick(Input input) {
		if (scrollText) {
			if (x1 > 35) {
				x1 -= 2;
			} else if (x2 < 70) {          // scrolls the intro text
				x2 += 2;
			} else {
				scrollText = false;
			}
		}
		
		if (input.togglekeys[input.controls.get("enter")] || input.togglekeys[input.controls.get("space")]) {
			if (index == 0) exit();
			if (index == menuOptions) startGame();
		}
		
		if (input.togglekeys[input.controls.get("up")]) index--;
		if (input.togglekeys[input.controls.get("down")]) index++;
		
		if (index < 0) index = menuOptions;
		if (index > menuOptions) index = 0;
	}
	
	private int left = 25, bottom = 125;
	private int gap = 15;
	
	@Override
	public void render(Screen screen) {
		screen.drawString("Welcome to", x1, y1, 11);
		screen.drawString("Target Practice by Nate", x2, y2, 14);
		
		if (!scrollText) {
			screen.drawString(">", left - 15, bottom - index * gap, 12);
			
			screen.drawString("Play game", left, bottom - gap * menuOptions, 12);
			screen.drawString("Exit", left, bottom, 12);
		}
	}
}
